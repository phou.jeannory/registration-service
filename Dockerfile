FROM java:8
ARG JAR_FILE=target/registration-service.jar
ARG JAR_LIB_FILE=target/lib/

# cd /usr/local/runme
WORKDIR /usr/local/runme

# cp target/registration-service.jar /usr/local/runme/registration-service.jar
COPY ${JAR_FILE} registration-service.jar

# cp -rf target/lib/  /usr/local/runme/lib
ADD ${JAR_LIB_FILE} lib/

# java -jar /usr/local/runme/registration-service.jar
ENTRYPOINT ["java","-jar","registration-service.jar"]